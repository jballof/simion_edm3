simion.workbench_program()

adjustable pe_update_each_usec      =   0.05    -- potential energy display
                                                -- update period (microsec)

-- quadrupole settings
adjustable _percent_tune            =    98.0   -- percent of optimum tune (typically just under 100)
adjustable _amu_mass_per_charge     =   100.0   -- mass/charge tune point (u/e)
adjustable _quad_entrance_voltage   =     0.0   -- quad entrance voltage
adjustable _quad_axis_voltage       =    -8.0   -- quad axis voltage
adjustable _quad_exit_voltage       =    -5.0   -- quad exit voltage
adjustable _quad_prefiltemode       =     1     -- if value is 1, then orefilter is biased AC only, else AC+DC
adjustable quad_phase_angle_deg     = 0.0       -- quad entry phase angle of ion (deg)
adjustable quad_frequency_hz        = 1.1E6     -- RF frequency of quad (Hz)

-- bender settings
adjustable _bender_kinEtune_eV    =    10.0  -- beam energy that is deflected / energy filter
adjustable _bender_einzel1_fact   =     1.0  -- additional scaling factor for bender einzel 1
adjustable _bender_einzel2_fact   =     1.0  -- additional scaling factor for bender einzel 2
adjustable _bender_corner1_fact   =     1.0  -- additional scaling factor for bencer corner electrode 1 
adjustable _bender_corner2_fact   =     1.0  -- additional scaling factor for bencer corner electrode 2
adjustable _bender_offset         =    -30   -- voltage offset for bender, including housing and outer lenses of Einzel

-- Temporary variables used internally.
local quad_eff_radius_cm       = 0.95 / 1.148  -- half the minimum distance between opposite rods (cm) ()
local quad_scaled_rf        -- a factor used in the RF component
local quad_omega            -- quad_frequency_hz (reexpressed in units of radians/usec)
local quad_theta            -- quad_phase_angle_deg (reexpressed in units of radians)
local last_pe_update = 0.0  -- last potential energy surface update time (usec)

-- Bender reference voltages for a beamenergy of 5 keV, will be scaled with tune point
local bendref03 = 1150         -- Einzel right (entrance)
local bendref04 = 1150         -- Einzel bottom
local bendref05 = -5000        -- Inner center electrode
local bendref06 = -3000        -- Inner corner electrode 
local bendref07 = 5000         -- Outer center  electrode 
local bendref08 = 3000         -- Outer corner electrode 

function segment.init_p_values()

    -- Set (non-AC) voltages as soon as ion enters PA

    -- Bender
    adj_elect01 = _bender_offset -- housing
    adj_elect03 = bendref03 * _bender_kinEtune_eV / 5000 * _bender_einzel1_fact + _bender_offset
    adj_elect04 = bendref04 * _bender_kinEtune_eV / 5000 * _bender_einzel2_fact + _bender_offset
    adj_elect05 = bendref05 * _bender_kinEtune_eV / 5000  + _bender_offset
    adj_elect06 = bendref06 * _bender_kinEtune_eV / 5000 * _bender_corner1_fact + _bender_offset
    adj_elect07 = bendref07 * _bender_kinEtune_eV / 5000  + _bender_offset
    adj_elect08 = bendref08 * _bender_kinEtune_eV / 5000 * _bender_corner2_fact + _bender_offset

    -- Entrance and exit lenses
    adj_elect25 = _quad_entrance_voltage
    adj_elect30 = _quad_exit_voltage

end

function segment.fast_adjust()

    if not quad_scaled_rf then
       -- pre-calculate values once for performance
        quad_scaled_rf = quad_eff_radius_cm^2 * quad_frequency_hz^2 * 7.222e-12
        quad_theta = quad_phase_angle_deg * (math.pi / 180)
        quad_omega = quad_frequency_hz * (1E-6 * 2 * math.pi)

        print("first fast adjust")
    end

    local quad_rfvolts = quad_scaled_rf * _amu_mass_per_charge
    local quad_dcvolts = quad_rfvolts * _percent_tune * ((1/100) * 0.1678399)
    local quad_temp_acvolts = sin(ion_time_of_flight * quad_omega + quad_theta) * quad_rfvolts 

    -- Pre- and Post-filter are typically supplied by AC only
    if _quad_prefiltemode == 1 then
        adj_elect26 = _quad_axis_voltage + quad_temp_acvolts 
        adj_elect27 = _quad_axis_voltage - quad_temp_acvolts 
    else
        adj_elect26 = _quad_axis_voltage + quad_temp_acvolts + quad_dcvolts
        adj_elect27 = _quad_axis_voltage - quad_temp_acvolts - quad_dcvolts
    end
    -- Middle part supplied by AC and DC
    adj_elect28 = _quad_axis_voltage + quad_temp_acvolts + quad_dcvolts
    adj_elect29 = _quad_axis_voltage - quad_temp_acvolts - quad_dcvolts

end


function segment.terminate()
    -- Put to anything higher than 0 to retain potentials set by code after "Fly'm"
    -- Potentials set in segment.fast_adjust are *not* retained, independently of the setting
   sim_retain_changed_potentials = 5
end


function segment.other_actions()
    if abs(ion_time_of_flight - last_pe_update) >= pe_update_each_usec then
        last_pe_update = ion_time_of_flight
        sim_update_pe_surface = 1    -- Request a PE surface display update.
    end
end

-- SIMION segment called by SIMION to override time-step size on each time-step.
function segment.tstep_adjust()
    -- Keep time step size below some fraction of the RF period.
    -- See "Time Step Size" comments.
    ion_time_step = min(ion_time_step, 0.1*1E+6/quad_frequency_hz)  -- X usec
 end