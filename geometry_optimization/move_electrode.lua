--[[
 move_electrode.lua
 Example to move electrode in PA# file without editing GEM file.
 Warning: not compatible as is with surface enhancement.
--]]

simion.workbench_program()

-- In PA/PA# file, moves electrode with given voltage by given offset dx,dy,dz.
-- warning: does not move any surface enhancement (if any)
-- From misc\pa_move_electrode.lua
function move(pa, v, dx,dy,dz)
  local x1    = dx < 0 and 0       or pa.nx-1
  local x2    = dx < 0 and pa.nx-1 or 0
  local xstep = dx < 0 and 1       or -1
  local y1    = dy < 0 and 0       or pa.ny-1
  local y2    = dy < 0 and pa.ny-1 or 0
  local ystep = dy < 0 and 1       or -1
  local z1    = dz < 0 and 0       or pa.nz-1
  local z2    = dz < 0 and pa.nz-1 or 0
  local zstep = dz < 0 and 1       or -1

  for xi = x1,x2,xstep do
  for yi = y1,y2,ystep do
  for zi = z1,z2,zstep do
    local vp, ep = pa:point(xi,yi,zi)
    if v == vp then
      pa:point(xi,yi,zi, 0, false)
      if xi+dx >= 0 and xi+dx < pa.nx and
         yi+dy >= 0 and yi+dy < pa.ny and
         zi+dz >= 0 and zi+dz < pa.nz
      then
        pa:point(xi+dx,yi+dy,zi+dz, vp, ep)
      end
    end
  end end end
end


local pa = simion.wb.instances[1].pa
-- simion.early_access(8.2) -- required if refine with disk==false

function segment.flym()
  sim_rerun_flym = 1 -- clears trajectories between runs
  for i=1,5 do
    pa:load('einzel.pa#')
    move(pa, 2, 0,-i,0)
    --pa:save('einzel2.pa#')
    pa:refine{convergence=1e-7 }
    -- pa:refine{convergence=1e-7, disk=false} -- alternately in-memory Refine (faster)
    pa:fast_adjust{[2]=100}
    run()
  end
end
