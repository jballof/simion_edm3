--[[
 bend.lua
 This code is normally disable but shows a few different approaches to
 rearrange PA instance order, particularly in regions where PA instances overlap.
--]]
simion.workbench_program()

local BEND_Y = 1
local BEND_X = 2
local BEND_XY = 3


-- uncomment below code to use instance_adjust in SIMION 8.2
-- (preferred method).
--[[
simion.early_access(8.2)
adjustable mode = 2
function segment.instance_adjust()
  if mode == 0 then
    -- enable BEND_XY as usual (no change)
  elseif mode == 1 then
    -- disable BEND_XY
    if ion_instance == BEND_XY then
      ion_instance = 0
    end
  elseif mode == 2 then
    -- disable BEND_XY and left side of BEND_X
    -- This should give essentially same result as mode == 0.
    if ion_instance == BEND_XY then
      ion_instance = 0
    elseif ion_instance == BEND_X then
      if (ion_px_mm - 50) < (ion_py_mm - 50) then
        ion_instance = 0	  
      end
    end
  end
end
function segment.other_actions()
  ion_color = ion_instance+1
  print(ion_instance)
end
--]]


-- uncomment below code to override efield_adjust, in SIMION 8.1
--[[
local e1 = simion.experimental.make_efield_adjust(
    function(x,y,z) return simion.wb.instances[BEND_Y]:field(x,y,z) end)
local e2 = simion.experimental.make_efield_adjust(
    function(x,y,z) return simion.wb.instances[BEND_X]:field(x,y,z, ion_time_of_flight) end)
function segment.efield_adjust()
  if ion_px_mm < 80 then e1() else e2() end
end
--]]


-- uncomment below code to do same as above but with time-dependent field.
--[[
simion.early_access(8.2)
local e1 = simion.experimental.make_efield_adjust(
    function(x,y,z) return simion.wb.instances[BEND_Y]:field(x,y,z, ion_time_of_flight) end)
local e2 = simion.experimental.make_efield_adjust(
    function(x,y,z) return simion.wb.instances[BEND_X]:field(x,y,z, ion_time_of_flight) end)
function segment.efield_adjust()
  if ion_px_mm < 80 then e1() else e2() end
end
--]]
