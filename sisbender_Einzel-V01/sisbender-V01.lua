simion.workbench_program()

 adjustable _bender_kinEtune_eV    =    10.0  -- beam energy that is deflected around the corner
 adjustable _bender_einzel1_fact   =     1.0  -- additional scaling factor for bender einzel 1
 adjustable _bender_einzel2_fact   =     1.0  -- additional scaling factor for bender einzel 1
 adjustable _bender_corner1_fact   =     1.0   
 adjustable _bender_corner2_fact   =     1.0

-- bender reference voltages for a beamenergy of 5 keV, will be scaled
local bendref01 = 0            -- housing and einzel ground
--local bendref02 = 1150         -- Einzel top 
local bendref03 = 1150         -- Einzel right (entrance)
local bendref04 = 1150         -- Einzel bottom
local bendref05 = -5000        -- Inner center electrode
local bendref06 = -3000        -- Inner corner electrode 
local bendref07 = 5000         -- Outer center  electrode 
local bendref08 = 3000         -- Outer corner electrode 


function segment.init_p_values()
    -- Set the voltages depending on the potential array, the ion is currently in.
    -- Setting voltages of an electrode that does not belong to the current PA
    -- will result in an error

    adj_elect01 = bendref01 * _bender_kinEtune_eV / 5000
  --  adj_elect02 = bendref02 * _bender_kinEtune_eV / 5000
    adj_elect03 = bendref03 * _bender_kinEtune_eV / 5000 * _bender_einzel1_fact
    adj_elect04 = bendref04 * _bender_kinEtune_eV / 5000 * _bender_einzel2_fact
    adj_elect05 = bendref05 * _bender_kinEtune_eV / 5000 
    adj_elect06 = bendref06 * _bender_kinEtune_eV / 5000 * _bender_corner1_fact
    adj_elect07 = bendref07 * _bender_kinEtune_eV / 5000 
    adj_elect08 = bendref08 * _bender_kinEtune_eV / 5000 * _bender_corner2_fact
  
end

function segment.terminate()
    -- Put to anything higher than 0 to retain potentials set by code after "Fly'm"
    -- Potentials set in fast_adjust are *not* retained, independently of the setting
   sim_retain_changed_potentials = 5
end

function segment.other_actions()
end
