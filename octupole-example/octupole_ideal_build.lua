-- Idealized linear octupole calculation,
-- and comparison to theory.
-- D.Manura, 2013-03

simion.pas:close() -- remove all PA's from RAM.

-- PA dimensions.
local pa = simion.pas:open()
pa:size(101,101,1)
pa.symmetry = '2dplanar[xy]'
pa.dx_mm = 0.5; pa.dy_mm = pa.dx_mm; pa.dz_mm = pa.dx_mm  -- mm/gu

local R0 = 25  -- inter-rode radius, mm
local Vmax = 100 -- rod voltage absolute value.

-- Theoretical potentials (for defining surface shape and
-- comparing Refine results to).  Position in mm.
local function theo(x,y,z)
  return Vmax * (x^4 + y^4 - 6 * x^2 * y^2) / R0^4
end

-- Theoretical field (for comparing Refine results to).  Position in mm.
local function etheo(x,y,z)
  local s = Vmax/R0^4
  return s*(-4*x^3 + 12*x*y^2), s*(-4*y^3 + 12*y*x^2), 0
end

-- Fill PA electrode points.
pa:fill{function(x,y,z)
  local v = theo(x,y,z)  
  if v >= Vmax then
    return Vmax, true
  elseif v <= -Vmax then
    return -Vmax, true
  else
    return 0, false
  end
end, surface='fractional'}

-- Calculation PA non-electrode points.
pa:refine{convergence=1e-7}

pa:save('octupole_ideal.pa')

-- Create PA of identical size/symmetry as given PA.
local function clone_pa(pa)
  local pa2 = simion.pas:open()
  pa2:size(pa:size())
  pa2.symmetry = pa.symmetry
  pa2.dx_mm = pa.dx_mm; pa2.dy_mm = pa.dy_mm; pa2.dz_mm = pa.dz_mm
  return pa2
end

-- Build PA containing difference in potential between
-- calculated and theory.
-- You can use PE/contour functions on the View screen to visualize this error.
local function compare(pa, theo, what, newfilename)
  local padiff = clone_pa(pa)
  for xg,yg,zg in padiff:points() do
    local x,y,z = xg*padiff.dx_mm, yg*padiff.dy_mm, zg*padiff.dz_mm
    local vtheo = theo(x,y,z)
    if pa:electrode(xg,yg,z) then
      padiff:potential(xg,yg,zg, 0)
    else
      local diff
      if what == 'potential' then
        diff = pa:potential(xg,yg,zg) - vtheo
      elseif what == 'field' then
        -- diff is the relative error: e * et / |et|^2 - 1
        local etx, ety, etz = etheo(x,y,z)
        local ex,ey,ez = pa:field_vc(xg,yg,zg)  -- V/gu
        ex = ex/pa.dx_mm; ey=ey/pa.dy_mm; ez=ez/pa.dz_mm  -- V/mm
        local cross = ex*etx + ey*ety + ez*etz
        local etm = math.sqrt(etx^2 + ety^2 + etz^2)
        diff = etm ~= 0 and cross/etm^2 - 1 or 0 
        -- if xg==1 and yg==0 then print(ex,ey,etx,ety,diff) end
      else
        error('invalid what')
      end
      padiff:potential(xg,yg,zg, diff)
    end
  end
  padiff.refinable = false  -- prevent asking to refine
  padiff.refined = true
  padiff:save(newfilename)
  return padiff
end

compare(pa, theo, 'potential', 'octupole_ideal_vdiff.pa')
compare(pa, theo, 'field',     'octupole_ideal_ediff.pa')
