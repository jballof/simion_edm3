# local mmgu = 0.4  -- PA cell size (mm/gu)
; Recommended values for mmgu:
; 0.1 for real octupole simulations (see AMS paper)
; 0.2 for some beam testing
; 0.4 for testing geometry

pa_define($(41/mmgu+1), $(41/mmgu+1), $(480/mmgu+1), planar,x, electric,, $(mmgu), surface=fractional)

;Electrode numbering offset, electrode numbers start with emin +1
#local emin = 20

;quadrupole variables definition
# local r0_quad = 3.0             -- inter-rod radius, mm
# local rrod_quad = r0_quad*1.148 -- circular rod radius, mm  (Dayton -- see README)

;octupole variables definition
# local r0 = 3    -- half min distance between opposite rods in mm
# local rrod = r0*0.355  -- rod radius in mm
# -- note: rrod/r0 = 0.355 according to [Rao 2000]
# --       for optimal octupole fields given circular rods

# local nx = math.floor((r0 + 2.5 * rrod) * mmgu) + 1

; move everying 10 mm in initial beam direction and 20 mm vertical
locate{0,20,10}
{
    ;the following geometry is line up such that the z axis is the central beam axis, x and y are mirrors

    ; put just something on the left

    e(28) {
        fill {
            within {box3d(-20 ,9,474 20,3,478 ) }
        }
    }

    ; octupule entrace region
    locate(0,0,1) {
    e(emin + 1) { ; plate with hole
        fill {
        within       { box3d(7.6,7.6, 0,-7.6,-7.6,1.0) }
        notin_inside { circle(0,0, 2.7) }
        }
    }
    }

    ;define octupole
    ;locate moves everything in positive z direction
    ;cylinder now from 85.4 to 5.8

    locate(0,0,204.0) {
    # for n=1,8 do
    #   local angle = (n - 1) * 45
        e($((n % 2) + 2 + emin)) {
            locate(0,0,0, 1, 0, $(angle)) {
            fill { within { cylinder(0, $(r0+rrod),0, $(rrod), , 200) } }
            }
        }
    # end
    }

    ; octupole exit region
    locate(0,0,206) {
    e(4 + emin) { ; exit enclosure
        fill {
        within       { box3d(7.6,7.6,0,   -7.6,-7.6,1.0) }
        notin_inside { circle(0,0, 2.7) }
        }
    }
    }

    ; quadrupole entrace region
    locate(0,0,212) {
    e(5 + emin) { ; plate with hole
        fill {
        within       { box3d(7.6,7.6, 0,-7.6,-7.6,1.0) }
        notin_inside { circle(0,0, 2.70) }
        }
    }
    }

    ; rods (four circular quad rods)
    locate(0,0,425) {
    e(6 + emin) {
        fill { within { cylinder($(r0_quad+rrod_quad),0,0, $(rrod_quad),, 210) } }
        fill { within { cylinder($(-r0_quad-rrod_quad),0,0, $(rrod_quad),, 210) } }
    }
    e(7 + emin) {
        fill { within { cylinder(0,$(r0_quad+rrod_quad),0, $(rrod_quad),, 210) } }
        fill { within { cylinder(0,$(-r0_quad-rrod_quad),0, $(rrod_quad),, 210) } }
    }
    }

    ; quadrupole exit region
    locate(0,0,427) {
    e(8 + emin) { ; exit enclosure
        fill {
        within       { box3d(7.6,7.6,0,   -7.6,-7.6,1.0) }
        notin_inside { circle(0,0, 2.7) }
        }
    }
    }
}