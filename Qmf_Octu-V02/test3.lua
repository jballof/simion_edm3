
--[[

SIMION workbech program for the FRIB-EDM3 frontend, exclusing ion funnels. This
script is based on the SIMION examples for a quadrupole and octupole. A 90 degrees
quadrupole bender is included, based on a supplied potential array from 
BeamImaging solutions.

06/12/2022  Jochen Ballof

--]]

 simion.workbench_program()

 -- parameters for switching between PAs
 adjustable z_trans_seg1_seg2     =     425  -- transition from segment 1 (quad) to segment 2 (bender)
 adjustable y_trans_seg2_seg3     =    -66   -- transition from segment 2 (bender) to segment 3 (octupole)
  
 adjustable _percent_tune          =    97.0  -- percent of optimum tune.
                                              -- (typically just under 100)
 adjustable _amu_mass_per_charge   =   100.0  -- mass/charge tune point (u/e)
                                              -- (particles of this m/z pass)
 adjustable _quad_entrance_voltage =     0.0  -- quad entrance voltage
 adjustable _quad_axis_voltage     =    -8.0  -- quad axis voltage
 adjustable _quad_exit_voltage     =  -100.0  -- quad exit voltage
 
 adjustable _detector_voltage      = -1500.0  -- detector voltage

 adjustable _octu1_entrance_voltage =     0.0  -- octupole 1 entrance voltage
 adjustable _octu1_exit_voltage     =     0.0  -- octupole 1 exit voltage
 
 adjustable _octu2_entrance_voltage =     0.0  -- octupole 2 entrance voltage
 adjustable _octu2_exit_voltage     =     0.0  -- octupole 2 exit voltage
 
 adjustable _bender_kinEtune_eV    =    10.0  -- beam energy that is deflected around the corner
 adjustable _bender_einzel1_fact   =     1.0  -- additional scaling factor for bender einzel 1
 adjustable _bender_einzel2_fact   =     1.0  -- additional scaling factor for bender einzel 1
 
 adjustable pe_update_each_usec    =   0.05   -- potential energy display
                                              -- update period (microsec)
                                              -- (for display purposes only)

 -- octupole 1 variables adjustable only at beginning of flight:
adjustable octu1_effective_radius_mm      = 3.0    -- half the minimum distance between
 -- opposite rods (mm)
adjustable _octu1_phase_angle_deg          = 0.0    -- entry phase angle of ion (deg)
adjustable _octu1_frequency_hz            = 1.1E6  -- RF frequency (Hz)

adjustable _octu1_rfvolts = 100    -- RF voltage for octu1pole
adjustable _octu1_dcvolts = 0      -- DC voltage for octu1pole;
                                     -- typically zero for RF-only octu1poles

 -- Quadrupole variables adjustable only at beginning of flight:
  
 adjustable quad_eff_radius_cm   = 0.95   -- half the minimum distance between
                                              -- opposite rods (cm)
 adjustable quad_phase_angle_deg          = 0.0    -- quad entry phase angle of ion (deg)
 adjustable quad_frequency_hz             = 1.1E6  -- RF frequency of quad (Hz)

 
 -- octupole 2 variables adjustable only at beginning of flight:
adjustable octu2_effective_radius_mm      = 3.0    -- half the minimum distance between
-- opposite rods (mm)
adjustable _octu2_phase_angle_deg         = 0.0    -- entry phase angle of ion (deg)
adjustable _octu2_frequency_hz            = 1.1E6  -- RF frequency (Hz)

adjustable _octu2_rfvolts = 100    -- RF voltage for octu1pole
adjustable _octu2_dcvolts = 0      -- DC voltage for octu1pole;
                                    -- typically zero for RF-only octu1poles

 
 -- Note: Using circular rods, the radius of the rods themselves
 -- should optimally be approximately 1.1487 * r_0.
 
 -- Temporary variables used internally.
 local quad_scaled_rf  -- a factor used in the RF component
 local quad_omega      -- quad_frequency_hz (reexpressed in units of radians/usec)
 local quad_theta      -- quad_phase_angle_deg (reexpressed in units of radians)
 local last_pe_update = 0.0 -- last potential energy surface update time (usec)

 -- bender reference voltages for a beamenergy of 5 keV, will be scaled
 local bendref01 = 0
 local bendref02 = 0
 local bendref03 = 5000         -- center electrode
 local bendref04 = -5000        -- center electrode
 local bendref05 = 1150         -- einzel (center) entrance
 local bendref06 = 0        
 local bendref07 = 1150         -- einzel (center) exit
 local bendref08 = 0
 local bendref09 = 3000         -- corner electrode
 local bendref10 = -3000        -- corner electrode

 -- define segment numbers
 local seg1 = 1     -- quadrupole and ion guide
 local seg2 = 2     -- bender
 local seg3 = 3     -- octupole 2
  
 -- SIMION segment called by SIMION at the start of ion flight for each potential
 -- array instance to initialize adjustable electrode voltages in that instance.
 -- NOTE: Code here can always more generally be placed instead in the
 --   fast_adjust segment.  Typically, the only reason for an
 --   init_p_values segment is to initialize static (not time-dependent)
 --   voltages all at once, avoiding executing code on every time-step as done
 --   in a fast_adjust segment.  This is intended to improve performance
 --   (though in some cases could reduce it).

 function segment.init_p_values()
     -- Set the voltages depending on the potential array, the ion is currently in.
     -- Setting voltages of an electrode that does not belong to the current PA
     -- will result in an error

    if ion_instance == seg2 then
        -- electron deflector
        adj_elect01 = bendref01 * _bender_kinEtune_eV / 5000
        adj_elect02 = bendref02 * _bender_kinEtune_eV / 5000
        adj_elect03 = bendref03 * _bender_kinEtune_eV / 5000
        adj_elect04 = bendref04 * _bender_kinEtune_eV / 5000
        adj_elect05 = bendref05 * _bender_kinEtune_eV / 5000 * _bender_einzel1_fact
        adj_elect06 = bendref06 * _bender_kinEtune_eV / 5000
        adj_elect07 = bendref07 * _bender_kinEtune_eV / 5000 * _bender_einzel2_fact
        adj_elect08 = bendref08 * _bender_kinEtune_eV / 5000
        adj_elect09 = bendref09 * _bender_kinEtune_eV / 5000
        adj_elect10 = bendref10 * _bender_kinEtune_eV / 5000
    elseif ion_instance == seg1 then
        -- first octupole and quadrupole
        adj_elect21 = _octu1_entrance_voltage
        adj_elect24 = _octu1_exit_voltage
        adj_elect25 = _quad_entrance_voltage
        adj_elect28 = _quad_exit_voltage

     elseif ion_instance == seg3 then
        -- last octupole
        adj_elect31 = _octu2_entrance_voltage
        adj_elect34 = _octu2_exit_voltage
        adj_elect35 = _detector_voltage
     end
 end
 
 -- SIMION segment called by SIMION to set adjustable electrode voltages
 -- in the current potential array instance.
 -- NOTE: this is called frequently, multiple times per time-step (by
 -- Runge-Kutta), so performance concerns here can be important.
 function segment.fast_adjust()
     -- See "Overview of Quad Equations" comments for details.
 
     if not quad_scaled_rf then
         -- Initialize constants if not already initialized.
         -- These constants don't change during particle flight,
         -- so we can calculate them once and reuse them.
         -- Reusing them is a bit more efficient (~25% by one estimate)
         -- than recalculating them on every fast_adjust call.
         quad_scaled_rf = quad_eff_radius_cm^2 * quad_frequency_hz^2 * 7.222e-12
         quad_theta = quad_phase_angle_deg * (math.pi / 180)
         quad_omega = quad_frequency_hz * (1E-6 * 2 * math.pi)

         print("first fast adjust")
     end

     -- Set the voltages depending on the potential array, the ion is currently in
     if ion_instance == seg1 then
        -- First octupole and quadrupole

        local quad_rfvolts = quad_scaled_rf * _amu_mass_per_charge
        local quad_dcvolts = quad_rfvolts * _percent_tune * ((1/100) * 0.1678399)
        local quad_tempvolts = sin(ion_time_of_flight * quad_omega + quad_theta) * quad_rfvolts + quad_dcvolts
 
        -- Finally, apply adjustable voltages to quad rod electrodes.
        adj_elect26 = _quad_axis_voltage + quad_tempvolts
        adj_elect27 = _quad_axis_voltage - quad_tempvolts

        -- do the same with the octu1pole
        local octu1_omega = _octu1_frequency_hz * (1E-6 * 2 * math.pi)
        local octu1_theta = _octu1_phase_angle_deg * (math.pi / 180)
    
        local octu1_tempvolts =
        sin(ion_time_of_flight * octu1_omega + octu1_theta) * _octu1_rfvolts + _octu1_dcvolts
    
        -- Apply adjustable voltages to rod electrodes.
        adj_elect22 =   octu1_tempvolts
        adj_elect23 = - octu1_tempvolts

     elseif ion_instance == seg3 then

        -- voltages for octupole 2
        local octu2_omega = _octu2_frequency_hz * (1E-6 * 2 * math.pi)
        local octu2_theta = _octu2_phase_angle_deg * (math.pi / 180)
    
        local octu2_tempvolts =
          sin(ion_time_of_flight * octu2_omega + octu2_theta) * _octu2_rfvolts + _octu2_dcvolts
    
        -- Apply adjustable voltages to rod electrodes.
        adj_elect32 =   octu2_tempvolts
        adj_elect33 = - octu2_tempvolts
     end
 end
 
 -- SIMION segment called by SIMION after every time-step.
 function segment.other_actions()
   -- Update potential energy surface display periodically.
   -- The performance overhead of this in non-PE views is only a few percent.
   -- NOTE: the value inside abs(...) can be negative when a new ion is flown.
   if abs(ion_time_of_flight - last_pe_update) >= pe_update_each_usec then
     last_pe_update = ion_time_of_flight
     sim_update_pe_surface = 1    -- Request a PE surface display update.
   end
 end
 
 -- SIMION segment called by SIMION to override time-step size on each time-step.
 function segment.tstep_adjust()
    -- Keep time step size below some fraction of the RF period.
    -- See "Time Step Size" comments.
    ion_time_step = min(ion_time_step, 0.1*1E+6/quad_frequency_hz)  -- X usec
 end
 
 function segment.terminate()
     -- Put to anything higher than 0 to retain potentials set by code after "Fly'm"
     -- Potentials set in fast_adjust are *not* retained, independently of the setting
    sim_retain_changed_potentials = 5
 end

 function segment.instance_adjust()
    -- changes priorities of segments during flight
    -- used to fix overlapping between instances
    if ion_instance == seg2 then
      -- switch ion from first (QMF) segment to bender segment if reasonably close
      if ion_pz_mm < z_trans_seg1_seg2 then
        ion_instance = 0  -- suppress, will query next instance
      end
    end

    if ion_instance == seg2 then
        -- switch ion from second (bender) segment to last ion guide if reasonable close
        if ion_py_mm < y_trans_seg2_seg3 then
           ion_instance = 0  -- suppress, will query next instance
        end
    end
  end

 