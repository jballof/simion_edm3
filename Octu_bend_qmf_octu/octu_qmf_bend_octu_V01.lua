simion.workbench_program()

-- Simulation for EDM3-Frontend --
-- ++++++++++++++++++++++++++++ -- 

-- This workbench program and (some) of the geometry files are based on SIMION
-- examples as provided with the program. The geometry has been adapted to mach
-- Extrel specifications. Some dimensions (e.g. exit and enrance lense spacing) 
-- were guessed. The bender geometry-file has been build based on the potential-
-- array supplied by BIS. The bender was fitted with circular einzel-lenses
-- inspired by the design of the Hiden-bender. 

-- Due to memory and cpu time requirements, the resolution was set to 0.4 mm/gpu.
-- The resolution might be too low for reliable results, especially for the small
-- octupole rods.

-- 06/02/2022
-- Jochen Ballof 
-- V01

adjustable pe_update_each_usec      =     0.05  -- potential energy display
                                                -- update period (microsec)

-- quadrupole settings
adjustable _percent_tune            =    98.0   -- percent of optimum tune (typically just under 100)
adjustable _amu_mass_per_charge     =   100.0   -- mass/charge tune point (u/e)
adjustable _quad_entrance_voltage   =     0.0   -- quad entrance voltage
adjustable _quad_axis_voltage       =    -8.0   -- constant voltage offset (same polarity for all rods)
adjustable _quad_exit_voltage       =    -5.0   -- quad exit voltage
adjustable _quad_prefiltemode       =     1     -- if value is 1, then prefilter is biased AC only, else AC+DC
adjustable quad_phase_angle_deg     =     0.0   -- quad entry phase angle of ion (deg)
adjustable quad_frequency_hz        =   1.1E6   -- RF frequency of quad (Hz)

-- bender settings
adjustable _bender_kinEtune_eV      =    10.0   -- beam energy that is deflected / energy filter
adjustable _bender_einzel1_fact     =     0.0   -- additional scaling factor for bender einzel 1 (entrance)
adjustable _bender_einzel2_fact     =     0.6   -- additional scaling factor for bender einzel 2 (exit)
adjustable _bender_corner1_fact     =     2.0   -- additional scaling factor for bender corner electrode 1 
adjustable _bender_corner2_fact     =     1.0   -- additional scaling factor for bender corner electrode 2
adjustable _bender_offset           =    -5.0   -- voltage offset for bender, including housing and outer lenses of Einzel

-- octupole 1 settings
adjustable octu1_phase_angle_deg    =     0.0   -- entry phase angle of ion (deg)
adjustable octu1_frequency_hz       =   1.1E6   -- RF frequency (Hz)
adjustable _octu1_rfvolts           =   100.0   -- RF voltage for octupole
adjustable octu1_dcvolts            =     0.0   -- DC voltage for octupole (typically zero)
adjustable _octu1_axis_voltage      =     0.0   -- constant voltage offset (same polarity for all rods)
adjustable _octu1_entrance_voltage  =     0.0   -- octu 1 entrance lens voltage
adjustable _octu1_exit_voltage      =     0.0   -- octu 1 exit lens voltage

-- octupole 1 settings
adjustable octu2_phase_angle_deg    =     0.0   -- entry phase angle of ion (deg)
adjustable octu2_frequency_hz       =   1.1E6   -- RF frequency (Hz)
adjustable _octu2_rfvolts           =   100.0   -- RF voltage for octupole
adjustable octu2_dcvolts            =     0.0   -- DC voltage for octupole (typically zero)
adjustable _octu2_axis_voltage      =    -5.0   -- constant voltage offset (same polarity for all rods)
adjustable _octu2_entrance_voltage  =    -5.0   -- octu 2 entrance lens voltage
adjustable _octu2_exit_voltage      =   -20.0   -- octu 2 exit lens voltage

-- detector settings
adjustable _detector_voltage        =   -30.0   -- voltage of 'detector'-plate after 2nd octupole

-- Temporary variables used internally.
local quad_eff_radius_cm        = 0.95 / 1.148  -- half the minimum distance between opposite rods (cm) ()
local quad_scaled_rf                            -- a factor used in the RF component
local quad_omega                                -- quad_frequency_hz (reexpressed in units of radians/usec)
local quad_theta                                -- quad_phase_angle_deg (reexpressed in units of radians)
local octu1_effective_radius_mm = 3.0           -- half the minimum distance between
 -- opposite rods (mm)
local last_pe_update = 0.0  -- last potential energy surface update time (usec)

-- Bender reference voltages for a beamenergy of 5 keV, will be scaled with tune point
local bendref03 = 1150         -- Einzel right (entrance)
local bendref04 = 1150         -- Einzel bottom (exit)
local bendref05 = -5000        -- Inner center electrode
local bendref06 = -3000        -- Inner corner electrode 
local bendref07 = 5000         -- Outer center electrode 
local bendref08 = 3000         -- Outer corner electrode 

-- variables for transmission calculation
local ntransmit = 0            -- particles reaching the detector 
local zcrossoctu1 = 241
local zcrossquad = 467
local ycrossbender = 238

local zlast = {}
local ylast = {}
local ntransoctu1 = 0
local ntransquad = 0
local ntransbender = 0
 
function segment.init_p_values()

    -- Set (non-AC) voltages as soon as ion enters PA

    -- Bender
    adj_elect01 = _bender_offset -- housing and outer lenses of Einzel
    adj_elect03 = bendref03 * _bender_kinEtune_eV / 5000 * _bender_einzel1_fact + _bender_offset
    adj_elect04 = bendref04 * _bender_kinEtune_eV / 5000 * _bender_einzel2_fact + _bender_offset
    adj_elect05 = bendref05 * _bender_kinEtune_eV / 5000  + _bender_offset
    adj_elect06 = bendref06 * _bender_kinEtune_eV / 5000 * _bender_corner1_fact + _bender_offset
    adj_elect07 = bendref07 * _bender_kinEtune_eV / 5000  + _bender_offset
    adj_elect08 = bendref08 * _bender_kinEtune_eV / 5000 * _bender_corner2_fact + _bender_offset

    -- Entrance and exit lenses
    adj_elect21 = _octu1_entrance_voltage
    adj_elect24 = _octu1_exit_voltage

    adj_elect25 = _quad_entrance_voltage
    adj_elect30 = _quad_exit_voltage

    adj_elect41 = _octu2_entrance_voltage
    adj_elect44 = _octu2_exit_voltage

    -- Detector voltage
    adj_elect45 = _detector_voltage

end

function segment.fast_adjust()

    if not quad_scaled_rf then
       -- pre-calculate values once for performance
        quad_scaled_rf = quad_eff_radius_cm^2 * quad_frequency_hz^2 * 7.222e-12
        quad_theta = quad_phase_angle_deg * (math.pi / 180)
        quad_omega = quad_frequency_hz * (1E-6 * 2 * math.pi)
    end

    local quad_rfvolts = quad_scaled_rf * _amu_mass_per_charge
    local quad_dcvolts = quad_rfvolts * _percent_tune * ((1/100) * 0.1678399)
    local quad_temp_acvolts = sin(ion_time_of_flight * quad_omega + quad_theta) * quad_rfvolts 

    -- Quadrupole pre- and post-filter are typically supplied by AC only
    if _quad_prefiltemode == 1 then
        adj_elect26 = _quad_axis_voltage + quad_temp_acvolts 
        adj_elect27 = _quad_axis_voltage - quad_temp_acvolts 
    else
        adj_elect26 = _quad_axis_voltage + quad_temp_acvolts + quad_dcvolts
        adj_elect27 = _quad_axis_voltage - quad_temp_acvolts - quad_dcvolts
    end

    -- middle part (mass filter) supplied by AC and DC
    adj_elect28 = _quad_axis_voltage + quad_temp_acvolts + quad_dcvolts
    adj_elect29 = _quad_axis_voltage - quad_temp_acvolts - quad_dcvolts

    -- octupole 1
    local octu1_omega = octu1_frequency_hz * (1E-6 * 2 * math.pi)
    local octu1_theta = octu1_phase_angle_deg * (math.pi / 180)
    
    local octu1_tempvolts =
        sin(ion_time_of_flight * octu1_omega + octu1_theta) * _octu1_rfvolts + octu1_dcvolts
    
    adj_elect22 =   octu1_tempvolts + _octu1_axis_voltage
    adj_elect23 = - octu1_tempvolts + _octu1_axis_voltage

    -- octupole 2
    local octu2_omega = octu2_frequency_hz * (1E-6 * 2 * math.pi)
    local octu2_theta = octu2_phase_angle_deg * (math.pi / 180)
    
    local octu2_tempvolts =
        sin(ion_time_of_flight * octu2_omega + octu2_theta) * _octu2_rfvolts + octu2_dcvolts
    
    adj_elect22 =   octu2_tempvolts + _octu2_axis_voltage
    adj_elect23 = - octu2_tempvolts + _octu2_axis_voltage
    
end

function segment.terminate()
    -- Put to anything higher than 0 to retain potentials set by code after "Fly'm"
    -- Potentials set in segment.fast_adjust are *not* retained, independently of the setting
    sim_retain_changed_potentials = 5

    -- Count transmitted ions
    if ion_py_mm < 25 and ion_pz_mm < 561 and ion_pz_mm > 551 then
        ntransmit = ntransmit + 1
    end
end

function segment.terminate_run()
    print('Total Transmission after first octu: ', ntransoctu1 / sim_ions_count * 100,'%')
    print('Total Transmission after quad: ', ntransquad / sim_ions_count * 100,'%')
    print('   Quad-only transmission: ', ntransquad / ntransoctu1 * 100, '%')
    print('Total Transmission after bender:', ntransbender / sim_ions_count * 100,'%')
    print('   Bender-only transmission:', ntransbender / ntransoctu1 * 100,'%')
    print('Transmission to detector:', ntransmit / sim_ions_count * 100, '%')

    -- print all variables to output
    -- todo: try to extend to global.
    local i = 1
    repeat
        local k, v = debug.getlocal(1, i)
        if k then
            print(k, v)
            i = i + 1
        end
        until nil == k
    end
  
function segment.other_actions()
    if abs(ion_time_of_flight - last_pe_update) >= pe_update_each_usec then
        last_pe_update = ion_time_of_flight
        sim_update_pe_surface = 1    -- Request a PE surface display update.
    end

    -- Count ions crossing the z-plane after the first octupole
    -- adapted from supplemental documentation
    if ((zlast[ion_number] or ion_pz_mm) < zcrossoctu1) and (ion_pz_mm >= zcrossoctu1) then
        ntransoctu1 = ntransoctu1 + 1
    elseif ((zlast[ion_number] or ion_pz_mm) < zcrossquad) and (ion_pz_mm >= zcrossquad) then
        ntransquad = ntransquad + 1
    elseif ((ylast[ion_number] or ion_py_mm) > ycrossbender) and (ion_py_mm <= ycrossbender) then
        ntransbender = ntransbender +1
    end
    
    zlast[ion_number] = ion_pz_mm
    ylast[ion_number] = ion_py_mm
    
end

-- SIMION segment called by SIMION to override time-step size on each time-step.
function segment.tstep_adjust()
    -- Keep time step size below some fraction of the RF period.
    -- See "Time Step Size" comments.
    ion_time_step = min(ion_time_step, 0.1*1E+6/quad_frequency_hz)  -- X usec
 end